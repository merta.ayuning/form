<?php 
if(isset($_POST['SUBMIT'])){
        $nama   = $_GET['nama'];
        $mapel  = $_GET['mapel'];
        $uts    = $_GET['uts'];
        $uas    = $_POST['uas'];
        $tugas  = $_POST['tugas'];
        $nilai_akhir = $_POST['nilai_akhir'];
        $grade =$_POST['grade'];

//penjumlahan dari nilai-nilai yang sudah diinput
$nilai_akhir =  $uts*0.35 + $uas*0.5 + $tugas*0.15;

//menampilkan grade berdasarkan hasil nilai akhir
if ($nilai_akhir>=90) {
  $grade = "A";
}
elseif ($nilai_akhir>=70) {
  $grade = "B";
}
elseif ($nilai_akhir>=50) {
  $grade = "C";
}
else {
  $grade = "D";
}
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Hasil Nilai</title>
  </head>
  <body>
  <?php if(isset($_POST['SUBMIT'])) ?>
        <div class="row justify-content-center">
            <div class="col-8 border border-primary mt-3 p-3">
                <div class="alert alert-success">
                Nama Siswa : <?php echo @$nama?> <br>
                Mata Pelajaran : <?php echo @$mapel?> <br>
                Nilai UTS : <?php echo @$uts?> <br>
                Nilai UAS :<?php echo @$uas?> <br>
                Nilai Tugas : <?php echo @$tugas?> <br>
                Nilai akhir  : <?php echo @$nilai_akhir?> <br>
                Grade : <?php echo @$grade?> <br>
                </div>
            </div>
        </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>